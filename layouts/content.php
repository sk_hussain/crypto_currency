<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CryptoCurrency_Theme
 */

?>



	<div class="col-sm-6 post_thumb">
	<article class="thumbnail" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			// the_content( sprintf(
			// 	/* translators: %s: Name of current post. */
			// 	wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'crypto_currency' ), array( 'span' => array( 'class' => array() ) ) ),
			// 	the_title( '<span class="screen-reader-text">"', '"</span>', false )
			// ) );

			the_excerpt();

			// wp_link_pages( array(
			// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'crypto_currency' ),
			// 	'after'  => '</div>',
			// ) );
		?>


	</div><!-- .entry-content -->

	<!-- <footer class="entry-footer">
		<?php crypto_currency_entry_footer(); ?>
	</footer> -->

	<a class="link_line" href="<?php echo the_permalink()?>">Learn More <i class="fa fa-angle-right "></i></a>


</article><!-- #post-## -->
	</div>




