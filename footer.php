<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CryptoCurrency_Theme
 */

?>

	</div><!-- #content -->

<footer id="main-footer" class="site-footer"><!--FOOTER BG COLOR -->
    <div class="container main-footer-area">
        <div class="row">
            <div class="col-sm-5">
            <?php 
            $footer_logo = get_field('logo_for_footer', option);
            if( !empty($footer_logo) ): ?>
                <img class="img-responsive" src="<?php echo $footer_logo['url']; ?>" alt="<?php echo $footer_logo['alt']; ?>" />
            <?php endif; ?>
               <div class="footer-info">
                   <?php the_field('copy_right_info', option)?>
               </div> 
            </div>
            <div class="col-sm-1 column_base">
            <?php
            // vars
            $getGroupData = get_field('column_1', option);	
            if( $getGroupData ): ?>
                <h5><?php echo $getGroupData['title_for_column']; ?></h5>
                    <?php $post_objects = $getGroupData['select_pages'] ?>
                    <?php
                    if( $post_objects ): ?>
                        <ul>
                        <?php foreach( $post_objects as $post_object): ?>
                            <li>
                                <a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a>
                                
                            </li>
                        <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
            <?php endif; ?>

            </div>


            <div class="col-sm-1 column_base">
            <?php
            // vars
            $getGroupData = get_field('column_2', option);	
            if( $getGroupData ): ?>
                <h5><?php echo $getGroupData['title_for_column']; ?></h5>
                    <?php $post_objects = $getGroupData['select_pages'] ?>
                    <?php
                    if( $post_objects ): ?>
                        <ul>
                        <?php foreach( $post_objects as $post_object): ?>
                            <li>
                                <a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a>
                                
                            </li>
                        <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
            <?php endif; ?>
                
            </div>


            <div class="col-sm-1 column_base">
            <?php
            // vars
            $getGroupData = get_field('column_3', option);	
            if( $getGroupData ): ?>
                <h5><?php echo $getGroupData['title_for_column']; ?></h5>
                    <?php $post_objects = $getGroupData['select_pages'] ?>
                    <?php
                    if( $post_objects ): ?>
                        <ul>
                        <?php foreach( $post_objects as $post_object): ?>
                            <li>
                                <a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a>
                                
                            </li>
                        <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
            <?php endif; ?>
                
            </div>


            <div class="col-sm-3 column_base">
            <?php
            // vars
            $getGroupData = get_field('column_4', option);	
            if( $getGroupData ): ?>
                <h5><?php echo $getGroupData['title_for_column']; ?></h5>
                    <?php echo $getGroupData['note_social'] ?>
                    
                    
            <?php endif; ?>
                
            </div>


        </div>
    </div><!-- close .container -->
</footer><!-- close #colophon -->




</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
