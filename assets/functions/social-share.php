<?php

add_shortcode('social_share', 'shortcode_social_share');

function shortcode_social_share($atts, $content = null) {
    extract( shortcode_atts( array(
		'btn_text'	=> 'Submit'
	), $atts ) );
    ob_start();
    ?>
<!-- HTML Code Goes Here -->    
<ul>
<?php

// check if the repeater field has rows of data
if( have_rows('social_repeater', option) ):

 	// loop through the rows of data
    while ( have_rows('social_repeater', option) ) : the_row(); ?>

        <li><a target="_blank" href="<?php the_sub_field('social_url', option); ?>" > <i class="<?php the_sub_field('icon_for_social_share', option); ?>"></i> <?php the_sub_field('name_for_social_share', option); ?></a></li>
    <?php endwhile;

else :

    // no rows found

endif;

?>
</ul>
<!-- END of HTML Code --> 


<?php
    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [social_share][/social_share] **/